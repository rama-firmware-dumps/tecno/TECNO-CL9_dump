#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_TECNO-CL9.mk

COMMON_LUNCH_CHOICES := \
    omni_TECNO-CL9-user \
    omni_TECNO-CL9-userdebug \
    omni_TECNO-CL9-eng
